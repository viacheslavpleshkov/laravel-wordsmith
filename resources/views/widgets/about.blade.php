<div class="col-six tab-full s-footer__about">
    <h4>{{ __('site.footer-about-wordsmith') }}</h4>
    <p>{{ $main->footer_about }}</p>
</div>